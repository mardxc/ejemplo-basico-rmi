/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package server;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.sql.SQLException;
import java.util.ArrayList;
import javax.swing.DefaultComboBoxModel;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Daryamz
 */
public interface Interface extends Remote  {
    public DefaultTableModel fillTable(String sql) throws SQLException, RemoteException;
    public DefaultComboBoxModel fillCombo(String sql) throws SQLException, RemoteException;
    public String registrar(String value, String tabla, String campo) throws SQLException, RemoteException;
    public String eliminar(Integer value, String tabla, String id) throws SQLException, RemoteException;
    public String actualizar(String value,String campo, String id, String tabla, String campoId) throws SQLException, RemoteException;
    public ArrayList max(String sql) throws SQLException, RemoteException;
    public void buscar(String value, String tabla, String campo) throws SQLException, RemoteException;
    ////////////////////////////////////////
    public String registrarPro(String value, String tabla, String campo) throws SQLException, RemoteException;
    public String actualizarPro(ArrayList value,ArrayList campo, String id, String tabla, String campoId) throws SQLException, RemoteException;
    public void IniciarHistorial(String fecha, String monto, String cliente) throws SQLException, RemoteException;
}
